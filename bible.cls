% archivo sacado de https://github.com/vermiculus/bible

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{bible}[2013/07/14 A LaTeX Class for typestting Bibles]
% \RequirePackage[l2tabu, orthodox]{nag}
\RequirePackage{xparse}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{memoir}}
\ProcessOptions\relax
\LoadClass{memoir}

\RequirePackage{geometry}
\geometry{top=2cm,bottom=3cm,inner=2.5cm,outer=1cm}

\let\footruleskip\relax
\RequirePackage{fancyhdr}
\fancyhead[LE,RO]{\@ifundefined{@currbook}{}{\@currbook}}

\RequirePackage{titlesec}
\titleformat{\section}[display]{\LARGE\filcenter}{}{}{}
\titleformat{\subsection}[display]{\large\bfseries}{}{}{}

\ExplSyntaxOn
\RequirePackage{xspace}

\NewDocumentCommand{\YWHW}{}{
  \textsc{Yahv\'{e}}\xspace
}

% Comandos para insertar los nombres de las otras traducciones de la Biblia citadas repetidamente en ésta traducción
\NewDocumentCommand{\nacar}{}{
  \emph{Nac\'{a}r\textendash{}Colunga}\xspace
}

\NewDocumentCommand{\bover}{}{
  \emph{Bover\textendash{}Cantera}\xspace
}

\NewDocumentCommand{\vulga}{}{
  \emph{Vulgata}\xspace
}

\NewDocumentCommand{\simon}{}{
  \emph{Simón\textendash{}Prado}\xspace
}

\newcounter{libro}
\newcounter{capitulo}
\newcounter{verso}

\setcounter{libro}{0}
\setcounter{capitulo}{0}
\setcounter{verso}{0}

\DeclareDocumentCommand{\libro}{m}{
  \chaptermark{#1}
  
  % Step the libro counter
  \refstepcounter{libro}
  
  % Don't display page numbers on pages that list a libro
  \thispagestyle{empty}

  \twocolumn[
  \begin{@twocolumnfalse}
    \begin{center}
      % Typeset main title
      \noindent{\HUGE #1}\vspace{1em}
    \end{center}
  \end{@twocolumnfalse}
  ]
  
  % Reset the chapter counter
  \setcounter{capitulo}{0}
}

\DeclareDocumentCommand{\verso}{}{
  % Step the verso counter
  \refstepcounter{verso}

  \ifnum\value{verso}>1\textsuperscript{\arabic{verso}}\else\hspace{6pt}\fi
}
\RequirePackage{xcolor} 
\RequirePackage{lettrine}
\DeclareDocumentCommand{\capitulo}{ o }{
  \refstepcounter{capitulo}
  \par\bigskip \if {#1} \lettrine[lines=3]{\arabic{capitulo}}{{#1}} \else \lettrine[lines=3]{\arabic{capitulo}}{} \fi
  
  % Reinicia el conteo de versos
  \setcounter{verso}{0}
}

\RequirePackage{xstring}
\newcommand{\testlibro}[1]{\def\inputlibro@only{#1}}
\newif\iftypeset
\typesettrue
\newcommand{\inputlibro}[1]{
  \iftypeset
  \begingroup
  \cs_if_exist:NTF{\inputlibro@only}
  {\IfStrEq{#1}{\inputlibro@only}{\input{\bibleversion/#1}\clearpage}}
  {\input{\bibleversion/#1}\clearpage}
  \endgroup
  \fi
}

\DeclareDocumentCommand{\bast}{m}{
  \emph{(#1)}\xspace
}

\ExplSyntaxOff

\newcommand{\SetVersion}[1]{\def\bibleversion{#1}}

% Local Variables:
% mode: latex
% TeX-master: "biblia.tex"
% TeX-engine: xetex
% TeX-PDF-mode: t
% End:
