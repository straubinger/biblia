__titulo: Cantar de los Cantares

Introducción

El misterio que Dios esconde en los amores entre esposo y esposa, y que presenta como figura en este divino Poema, no ha sido penetrado todavía en forma que permita explicar satisfactoriamente el sentido propio de todos sus detalles. El breve libro es sin duda el más hondo arcano de la Biblia, más aún que el Apocalipsis, pues en éste, cuyo nombre significa revelación, se nos comunica abiertamente que el asunto central de su profecía es la Parusía de Cristo y los acontecimientos que acompañarán aquel supremo día del Señor en que Él se nos revelará para que lo veamos “cara a cara”. Aquí, en cambio, se trata de una gran Parábola o alegoría en la cual, excluida como se debe la interpretación mal llamada histórica, que quisiera ver en ella un epitalamio vulgar y sensual, aplicándolo a s Salomón y la princesa de Egipto, no tenemos casi referencias concretas, salvo alguna (cf. 6, 4 y nota), que permite con bastante firmeza ver en la Amada a Israel, esposa de Yahvé.

La diversidad casi incontable de las conclusiones propuestas por los que han investigado el sentido propio del Cántico, basta para mostrar que la verdad total no ha sido descubierta. No sabemos con certeza si el Esposo es uno solo, o si hay varios, que podrían ser un rey y un pastor como pretendientes de Israel (Vaccari), o podrían ser, paralelamente, Yahvé (el Padre) como Esposo de Israel, y Jesucristo como Esposo de la Iglesia ya preparada para las bodas del Cordero que veremos en Apocalipsis 19, 6-9. Ignoramos también qué ciudad es ésa en que la Esposa sale por dos veces a buscar al Amado. Ignoramos principalmente cuál es el tiempo en que ocurre u ocurrirá la acción del pequeño gran drama, y ni siquiera podemos afirmar en todos los casos (pues las opiniones también varían en esto) cuál de los personajes es el que habla en cada momento del diálogo.

En tal situación, después de mucho meditar, hemos llegado a la conclusión de que es forzoso ser muy parco en afirmaciones con respecto al Cantar. Porque no está al alcance del hombre explicar los misterios que Dios no ha aclarado aún a la Iglesia, y sería vano estrujar el entendimiento para querer penetrar, a fuerza de inteligencia pura, lo que Dios se complace en revelar a los pequeños. Sería, en cambio, tremenda responsabilidad delante de Él, aseverar como verdades reveladas lo que no fuese sino producto de nuestra imaginación o de nuestro deseo, como lo hicieron esos falsos profetas tantas veces fustigados por Jeremías y otros videntes de Dios.

Como enseña el Eclesiástico (cf. 39, 1 ss. y nota), nada es más propio del verdadero sabio según Dios, que investigar las profecías y el sentido oculto de las parábolas: tal es la parte de María, que Jesús declaró ser la mejor. Pero esa misma palabra de Dios, cuya meditación ha de ocuparnos “día y noche” (Salmo 1, 2), nos hace saber que hay cosas que sólo se entenderán al fin de los tiempos (Jeremías 30, 24). El mismo Jeremías, refiriéndose a estos misterios y a la imprudencia de querer explicarlos antes de tiempo, dice: “Al fin de los tiempos conoceréis sus designios” (de Dios). Y agrega inmediatamente, cediendo la palabra al mismo Dios: “Yo no enviaba a esos profetas, y ellos corrían. No les hablaba, y ellos profetizaban” (Jeremías 23, 20-21). En Daniel encontramos sobre esto una notable confirmación. Después de revelársele, por medio del Ángel Gabriel, maravillosos arcanos sobre los últimos -tiempos, entre los cuales vemos la grande hazaña de San Miguel Arcángel defensor de Israel (Dan. 12, 1; cf. Apocalipsis 12, 7), se le dice: “Pero tú, oh Daniel, ten en secreto estas palabras y sella el Libro hasta el tiempo del fin” (Dan. 12, 4). Y como el Profeta insistiese en querer descubrirlo, tornó a decir el Ángel: “Anda, Daniel, que esas cosas están cerradas y selladas hasta el tiempo del fin” (ibíd. 9). Entonces “ninguno de los malvados entenderá, pero los que tienen entendimiento comprenderán” (ibíd. 10). Finalmente, vemos que aún en la profecía del Apocalipsis, cuyas palabras se le prohibió sellar a San Juan (Apocalipsis 22,10), hay sin embargo un misterio, el de los siete truenos, cuyas voces le fue vedado revelar (Apocalipsis 10, 4).

Nuestra actitud, pues, ha de ser la que enseña el Espíritu Santo al final del mismo Apocalipsis, fulminando terribles plagas sobre los que pretendan añadir algo a sus palabras, y amenazando luego con excluir del Libro de la vida y de todas las bendiciones anunciadas por el vidente de Patmos, a los que disminuyan las palabras de su profecía (Apocalipsis 22,18 s.).

El criterio expuesto así, a la luz de la misma Escritura, nos muestra desde luego que, si es hermoso aplicar a la Virgen María, como hace la liturgia, los elogios más ditirámbicos que recibe la Esposa del Cantar, pues que ciertamente nadie pudo ni podrá merecerlos más que Aquélla a quien el Ángel declaró bendita entre las mujeres, no es menos cierto que hemos de evitar la tentación de generalizar y ver en María a la protagonista del Cántico, incluso en aquella incidencia del capítulo 5 en que la Esposa rehúsa abrir la puerta al Esposo por no ensuciarse los pies. Semejante infidelidad jamás podría atribuirse a la Virgen Inmaculada, ni aun cuando en esa escena se tratase de un sueño, como algunos interpretan. Basta recordar la actitud de María ante la Anunciación del Ángel, en la cual, si bien Ella afirma su voto de virginidad, en manera alguna cierra la puerta a la Encarnación del Verbo; antes por el contrario, Cristo, lejos de sentirse rechazado como el Esposo del Cantar, realiza el estupendo prodigio de penetrar virginalmente en el huerto cerrado del seno maternal. Y es por igual razón que esa falla de la Esposa no puede atribuirse tampoco a la Iglesia cristiana como esposa del Cordero, así como también resultan inaplicables a ella los caracteres de esposa repudiada y perdonada, con que los profetas señalan repetidamente a Israel (Isaías 54, 1 y nota).

De ahí que, por eliminación —y sin perjuicio de las preciosas aplicaciones místicas al alma cristiana, las cuales, como bien observa Joüon, en ningún caso pretenden ser una interpretación del sentido propio del poema bíblico— hemos de inclinarnos en general a admitir en él, como han hecho los más autorizados comentadores antiguos y modernos, lo que se llama la alegoría yahvística, o sea los amores nupciales entre Dios e Israel, a la luz del misterio mesiánico, a pesar de que tampoco en ella nos es posible descubrir en detalle el significado propio de cada uno de los episodios de este divino Epitalamio. “A esta sentencia fundamental (sobre Israel) nos debemos atener”, dice en su introducción al poema la Biblia española de Nácar-Colunga, y agrega inmediatamente: “Pero admitido este principio, una duda salta a la vista. Los historiadores sagrados y los profetas están concordes en pintarnos a Israel como infiel a su Esposo y manchada de infinitos adulterios; lo cual no está conforme con el Cántico, donde la Esposa aparece siempre enamorada de su Esposo, y además, toda hermosa o pura. La solución a esta dificultad nos la ofrecen los mismos profetas cuando al Israel histórico oponen el Israel de la época mesiánica, purificado de sus pecados y vuelto de todo corazón a su Dios. Las relaciones rotas por el pecado de idolatría se reanudan para siempre. Es preciso, pues, decir que el Cántico celebra los amores de Yahvé y de Israel en la edad mesiánica, que es el objeto de los deseos de los profetas y justos del Antiguo Testamento. En torno a esta imagen del matrimonio, usada por los profetas, reúne el sabio todas las promesas contenidas en los escritos proféticos” (cf. Éxodo 34, 16; Números 14, 34; Isaías 54, 4 ss.; 62, 4 ss.; Oseas 1, 2; 2, 4 y 19; 6, 10; Jeremías 2, 2; 3, 1 y 2; 3, 14; Ezequiel 16).

El Sumo Pontífice Pío XII, en su importantísima Encíclica “Divino Afflante Spiritu”, sobre los estudios bíblicos alude expresamente a las dificultades de interpretación que dejamos planteadas, al decir que “no pocas cosas... apenas fueron explicadas por los expositores de los pasados siglos”; que “entre las muchas cosas que se proponen en los Libros sagrados legales, históricos, sapienciales y proféticos, sólo muy pocas hay cuyo sentido haya sido declarado por la autoridad de la Iglesia,, y no son muchas más aquellas en las que sea unánime la sentencia de los Santos Padres” y que “si la deseada solución se retarda por largo tiempo, y el éxito feliz no nos sonríe a nosotros, sino que acaso se relega a que lo alcancen los venideros, nadie por eso se incomode... siendo así que a veces se trata de cosas oscuras y demasiado lejanamente remotas de nuestros tiempos y de nuestra experiencia”.

Entretanto, y a pesar de nuestra ignorancia actual para fijar con certeza el sentido propio de todos sus detalles, el divino poema nos es de utilidad sin límites para nuestra vida espiritual, pues nos lleva a creer en el más precioso y santificador de los dogmas: el amor que Dios nos tiene, según esa inmensa verdad sobrenatural que expresó, a manera de testamento espiritual, el Beato Pedro Julián Eymard: “La fe en el amor de Dios es la que hace amar a Dios.”

No puede haber la menor duda de que sea lícito a cada alma creyente recoger para sí misma las encendidas palabras de amor que el Esposo dirige a la Esposa. El Cantar es, en tal sentido, una celestial maravilla para hacernos descubrir y llevarnos a lo que más nos interesa, es decir, a creer en el amor con que somos amados. El que es capaz de hacerse bastante pequeño para aceptar, como dicho a sí mismo por Jesús, lo que el Amado dice a la Amada, siente la necesidad de responderle a Él con palabras de amor, y de fe, y de entrega ansiosa, que la Amada dirige al Amado. Felices aquellos que exploten este sublime instrumento, que es a un tiempo poético y profético, como los Salmos de David, y en el cual se juntan, de un modo casi sensible, la belleza y la piedad, el amor y la esperanza, la felicidad y la santidad. ¡Y felices también nosotros si conseguimos darlo en forma que pueda ser de veras aprovechado por las almas!

El título “Cantar de los Cantares” (en hebreo Schir Haschirim) equivale, en el lenguaje bíblico, a un superlativo como “vanidad de vanidades” (Eclesiastés 1, 2), “Rey de Reyes y Señor de Señores” (Apocalipsis 19, 16), etc., y quiere decir que esta canción es superior a todas. “El Alto Canto” se le llama en alemán; en italiano “La Cántica” por antonomasia, etc. Efectivamente el “Cantar de los Cantares” ha ocupado y sigue ocupando el primer lugar en la literatura mística de todos los siglos.

Poema todo oriental, no puede juzgárselo, como bien dice Vigouroux, según las reglas puestas por los griegos, como son las nuestras. Tiene unidad, pero “entendida a la manera oriental, es decir, mucho más en el pensamiento inspirador que en la ejecución de la obra”.

Intervienen en el “Cantar de los Cantares”, mediante diálogos y a veces en forma dramática, la Esposa (Sulamita) y el Esposo, denominados también en ocasiones hermano y hermana. Aparecen además otros personajes: los “hermanos”, las “hijas de Jerusalén”, etc., que forman algo así como el coro de la antigua tragedia griega. La manera en que se tratan el Amado y la Amada muestra claramente que no son simples amantes, porque entre los israelitas solamente los esposos podían tratarse tan estrechamente.

No se exhibe, pues, aquí un amor prohibido o culpable, sino una relación legítima entre esposos. A este respecto debe advertirse desde luego que el lenguaje del Cántico es el de un amor entre los sexos. No creemos que esto haya de explicarse solamente porque se trata de un poema de costumbres orientales, sino también porque la Biblia es siempre así: “plata probada por el fuego, purificada de escoria, siete veces depurada” (Salmo 11, 7). Ella dice todo lo que debe decir, sin el menor disimulo (cf. Génesis 19, 30 y nota), es decir, como muy bien observa Hello, sin revestir la verdad con apariencias que atraigan el aplauso de los demás, según suelen hacer los hombres. Dios quiere aplicar aquí, a los grandes misterios de su amor con la humanidad —ya se trate de Israel, de la Iglesia o de cada alma— la más vigorosa de las imágenes: la atracción de los sexos. Sabe que todos la comprenderán, porque todos la sienten. Y en ello no ha de verse lo prohibido, sino lo legítimo del amor matrimonial, instituido por Dios mismo, a la manera como el vino sólo sería malo en el ebrio que lo bebiera pecaminosamente. De ahí que, como muy bien se ha dicho de este sublime poema, “el que vea mal en ello, no hará sino poner su propia malicia. Y el que sin malicia lo lea buscando su alimento espiritual, hallará el más precioso antídoto contra la carne”.

Los expositores antiguos miraron siempre como autor del libro al rey Salomón cuyo nombre figura en el título: “Cantar de los Cantares de Salomón” y fue respetado por el traductor griego. La Vulgata no pone nombre de autor, y diversos exégetas católicos remiten la composición del Cantar a tiempos posteriores a Salomón (Joüon, Holzhey, Ricciotti, Zapletal, etc.). Otros empero, entre ellos Fillion, lo atribuyen al mismo rey sabio, que en el poema figura con toda su opulencia. A este respecto no podemos dejar de señalar, entre las muchas interpretaciones (que hacen variar de mil maneras el diálogo y el sentido, según que pongan cada versículo en boca de uno u otro de los personajes), la que adopta un estudioso tan autorizado como Vaccari presentándola como “la que mejor corresponde, tanto a los datos intrínsecos del Libro, cuanto a las condiciones históricas del antiguo Israel”. Según esta interpretación, el Esposo a quien ama la Sulamita, no es la misma persona que el rey, sino un joven pastor que la celebra en un lenguaje idílico y agreste, contrastando precisamente con la fastuosidad del rey cuyas atracciones desprecia la Esposa que prefiere a su Amado. En este contraste, la paz del campo simboliza la Religión de Israel, tan sencilla como verdadera, y los esplendores de la Corte figuran los de la civilización pagana, que humanamente hablando parece tan superior a la hebrea. Tendríamos así, como en las dos Ciudades de San Agustín, el eterno contraste entre Dios y el mundo, entre lo espiritual y lo temporal. El valor de esta interpretación que permite entender muchos pasajes antes obscuros, podrá juzgarse a medida que la señalemos en las notas. Entretanto ella explicaría que Salomón, siendo el autor del Poema (como lo sostiene también Vigouroux con sólidas razones) se haya puesto él mismo como personaje del drama, pues que, siendo así, ya no aparecería como figura del divino Esposo, sino que, lejos de ello, se presenta modestamente con su persona y su proverbial opulencia, como un ejemplo de la vanidad de todo lo terreno, cosa muy propia de la sabiduría de aquel gran Rey.

Agreguemos que esta manera de entender el Cantar según lo propone Vaccari no se opone en modo alguno al aprovechamiento de su riquísima doctrina mística, pues nada más congruente que aplicar las relaciones de Yahvé con su esposa Israel, a las de su Hijo Jesús, espejo perfectísimo del Padre (Hebreos 1, 3), con la Iglesia que Él fundó, y con cada una de las almas que la forman, en su peregrinación actual en busca del Esposo (cf. 4, 7; 3, 3; 5, 6 y notas); en la misteriosa unión anticipada de la vida eucarística (cf. 2, 6 y nota); y finalmente en su bienaventurada esperanza (cf. 1, 1; 8, 13 s. y notas; Tito 2, 13), cuya realización anhela ella desde el principio con un suspiro que no es sino el que repetimos cada día en el Padre Nuestro enseñado por el mismo Cristo: “Adveniat Regnum tuum”, y el que los primeros cristianos exhalaban en su oración que desde el siglo primero nos ha conservado la “Didajé” o “Doctrina de los doce Apóstoles”: “Así como este pan fraccionado estuvo disperso sobre las colinas y fue recogido para formar un todo, así también, de todos los confines de la tierra, sea tu Iglesia reunida para el Reino tuyo... líbrala de todo mal, consúmala en tu caridad, y de los cuatro vientos reúnela, santificada, en tu reino que para ella preparaste, porque tuyo es el poder y la gloria en los siglos. ¡Venga ¡a gracia! ¡Pase este mundo! ¡Hosanna al Hijo de David! Acérquese el que sea santo; arrepiéntase el que no lo sea. Maranatha (Ven Señor). Amén.”

Para facilitar la lectura, orientando al lector, señalamos aquí la división en seis escenas que propone Vaccari y sintetizamos brevemente el contenido de cada una de ellas:

ESCENA I (1, 1-2, 7): a) El anhelo de la Esposa (1, 1-14): Ella busca al Amado y él le indica el campo. El rey la solicita. Ella prefiere al pastor, b) El primer encuentro (1,15-2, 7): Dialogo y unión de los dos esposos.

ESCENA II (2, 8-3, 5): a) En el campo (2, 8-17): Invitación del Esposo y paseo campestre. b) Búsqueda nocturna del Esposo (3, 1-5): .Ella recorre en vano la ciudad. Lo encuentra afuera.

ESCENA III (3, 6-5, 1): a) “Salomón en todo su esplendor” (3, 6-11): Coro sobre la opulencia del rey (tentación), b) Retrato de la Esposa (4, 1-6). c) El místico jardín (4, 7-5, 1): El Amado le hace el gran elogio. Ella se goza. Él invita a los amigos.

ESCENA IV (5, 2-6, 3): a) Visita nocturna (5, 2-9): La Esposa no abre al Amado. Luego lo busca en vano, b) Ella hace la semblanza del Esposo ante el coro (5, 10-6, 3).

ESCENA V, (6, 4-8, 4): a) Nuevas loas de la Esposa (6, 4-7, 1). b) Justa de requiebros, en que parecen rivalizar el rey y el pastor (7, 2-10). c) Fidelidad de la Esposa (7, 11-8, 4).

ESCENA VI (8, 5-14): a) El triunfo del amor (8, 5-7): La Esposa descansa en el Amado. El fuego divino. Unión transformante. b) Parábolas de la hermanita y de la viña (8, 8-12). c) Idilio (8, 13) y llamado final (8, 14).

 

 

 

Cantar de los Cantares 1

Cantar de los Cantares, de Salomón

Esposa

^({versiculo}){nota al pie:?} ¡Béseme él con los besos de su boca!

porque tus amores son mejores que el vino.

^({versiculo}){nota al pie:?}Suave es el olor de tus ungüentos;

es tu nombre ungüento derramado;

por eso te aman las doncellas.

Coro

^({versiculo}){nota al pie:?}Atráeme en pos de ti. ¡Corramos!

Me introdujo el Rey en sus cámaras.

Nos gozaremos, nos alegraremos en ti.

Celebraremos tus amores más que el vino.

Con razón te aman.

Esposa

^({versiculo}){nota al pie:?}Morena soy, pero hermosa,

oh hijas de Jerusalén,

como las tiendas de Cedar,

como los pabellones de Salomón.

^({versiculo}){nota al pie:?}No reparéis en que soy morena;

es que me ha quemado el sol.

Los hijos de mi madre se airaron contra mí;

me pusieron a guardar las viñas;

pero mi viña, la mía, no he guardado.

^({versiculo})Dime, oh tú a quien ama el alma mía,

dónde pastoreas,

dónde haces sestear las ovejas al mediodía,

para que no ande yo vagando

alrededor de los rebaños de tus compañeros.

Esposo o Coro

^({versiculo}){nota al pie:?}Si no lo sabes, oh hermosa entre las mujeres,

sal siguiendo las huellas del rebaño,

y apacienta tus cabritos

junto a las cabañas de los pastores.

Esposo

^({versiculo}){nota al pie:?}A mi yegua, en las carrozas del Faraón,

te comparo, oh amiga mía.

^({versiculo})Hermosas son tus mejillas entre los pendientes,

cuello entre los collares.

^({versiculo}){nota al pie:?}Collares de oro haremos para ti

incrustados de plata.

Esposa

^({versiculo}){nota al pie:?}Estando el rey en su diván,

mi nardo exhala su fragancia.

^({versiculo}){nota al pie:?}Un manojito de mirra

es para mí el amado mío:

reposa entre mis pechos.

^({versiculo}){nota al pie:?}Racimo de cipro

es mi amado para mí

en las viñas de Engadí.

Esposo

^({versiculo}){nota al pie:?}Hermosa eres, amiga mía,

eres hermosa;

tus ojos son palomas.

Esposa

^({versiculo})Hermoso eres, amado mío, ¡y cuan delicioso!

y nuestro lecho es de flores.

Esposo

^({versiculo})De cedro son las vigas de nuestra casa,

de ciprés nuestros artesonados.

 

Cantar de los Cantares 2

Esposa

^({versiculo}){nota al pie:?}Yo soy el lirio de Sarón,

la azucena de los valles.

Esposo

^({versiculo}){nota al pie:?}Como una azucena entre los espinos,

así, es mi amiga entre las doncellas.

Esposa

^({versiculo}){nota al pie:?}Como el manzano entre los árboles silvestres,

tal es mi amado entre los mancebos.

A su sombra anhelo sentarme,

y su fruto es dulce a mi paladar.

^({versiculo}){nota al pie:?}Me introdujo en la celda del vino,

y su bandera sobre mí es el amor.

^({versiculo}){nota al pie:?}¡Confortadme con pasas!

¡Restauradme con manzanas!

porque languidezco de amor.

^({versiculo}){nota al pie:?}Su izquierda está debajo de mi cabeza,

y su derecha me abraza.

Esposo

^({versiculo}){nota al pie:?}Os conjuro, oh hijas de Jerusalén,

por las gacelas y las ciervas del campo,

que no despertéis ni inquietéis a la amada,

hasta que ella quiera.

Esposa

^({versiculo}){nota al pie:?}¡La voz de mi amado!

Helo aquí que viene,

saltando por los montes,

brincando sobre los collados.

^({versiculo})Es mí amado como el gamo,

o como el cervatillo.

Vedlo ya detrás de nuestra pared,

mirando por las ventanas,

atisbando por las celosías.

^({versiculo}){nota al pie:?}Habla mi amado, y me dice:

Esposo

Levántate, amiga mía; hermosa mía, ven.

^({versiculo}){nota al pie:?}Porque, mira, ha pasado ya el invierno,

la lluvia ha cesado y se ha ido;

^({versiculo})aparecen ya las flores en la tierra;

llega el tiempo de la poda,

y se oye en nuestra tierra

la voz de la tórtola.

^({versiculo}){nota al pie:?}Ya echa sus brotes la higuera,

esparcen su fragancia las viñas en flor.

¡Levántate, amiga mía;

hermosa mía, ven!

^({versiculo}){nota al pie:?}Paloma mía,

que anidas en las grietas de la peña,

en los escondrijos de los muros escarpados,

hazme ver tu rostro,

déjame oír tu voz;

porque tu voz es dulce,

y tu rostro es encantador.

Esposa

^({versiculo}){nota al pie:?}Cazadnos las raposas,

las raposillas que devastan las viñas,

porque nuestras viñas están en flor.

^({versiculo}){nota al pie:?}Mi amado es mío,

y yo soy suya;

él apacienta entre azucenas.

^({versiculo}){nota al pie:?}Mientras sopla la brisa,

y se alargan las sombras,

¡vuélvete, amado mío!

¡Aseméjate al gamo,

o al cervatillo,

sobre los montes escarpados!

 

Cantar de los Cantares 3

Esposa

^({versiculo}){nota al pie:?}En mi lecho, de noche,

busqué al que ama mi alma;

le busqué y no le hallé.

^({versiculo})Me levantaré, pues,

y giraré por la ciudad,

por las calles y las plazas;

buscaré al que ama mi alma.

Le busqué y no le hallé.

^({versiculo}){nota al pie:?}^(\ )Me encontraron los guardias

que hacen la ronda por la ciudad:

“¿Habéis visto al que ama mi alma?”

^({versiculo}){nota al pie:?}Apenas me había apartado de ellos,

encontré al que ama mi alma.

Lo así y no lo soltaré

hasta introducirlo en la casa de mi madre,

y en la cámara de la que me dio el ser.

Esposo (¿o Pastor?)

^({versiculo}){nota al pie:?}Os conjuro, oh hijas de Jerusalén,

por las gacelas y las ciervas del campo,

que no despertéis ni inquietéis a la amada,

hasta que ella quiera.

Coro

^({versiculo}){nota al pie:?}¿Qué cosa es esta que sube del desierto,

como columna de humo

perfumada de mirra e incienso

con todos los aromas del mercader?

^({versiculo}){nota al pie:?}Mirad, es su litera, la de Salomón;

sesenta valientes la rodean,

de entre los héroes de Israel.

^({versiculo})Todos ellos manejan la espada,

son adiestrados para el combate;

todos llevan la espada ceñida,

a causa de los peligros de la noche.

^({versiculo})De maderas del Líbano

se hizo el rey Salomón un cenáculo.

^({versiculo})Hizo de plata sus columnas,

de oro el dosel,

de púrpura su asiento;

su interior está recamado de amor,

por las hijas de Jerusalén.

^({versiculo}){nota al pie:?}Salid, oh hijas de Sión,

a contemplar al rey Salomón

con la corona que le tejió su madre

en el día de sus desposorios,

el día del gozo de su corazón.

 

Cantar de los Cantares 4

Esposo

^({versiculo}){nota al pie:?}¡Qué hermosa eres, amiga mía!

¡Cuán hermosa eres tú!

Tus ojos son palomas, detrás de tu velo.

Tu cabellera es como un rebaño de cabras,

que va por la montaña de Galaad.

^({versiculo})Son tus dientes

como hatos de ovejas esquiladas,

que suben del lavadero,

todas con crías mellizas,

sin que haya entre ellas una estéril.

^({versiculo}){nota al pie:?}Como cinta de púrpura son tus labios,

y graciosa es tu boca.

Como mitades de granada son tus mejillas,

detrás de tu velo.

^({versiculo}){nota al pie:?}Tu cuello es cual la torre de David,

construida para armería,

de la que penden mil escudos,

todos ellos arneses de valientes.

^({versiculo}){nota al pie:?}Como dos mellizos de gacela

que pacen entre azucenas,

son tus dos pechos.

Esposa

^({versiculo}){nota al pie:?}Mientras sopla la brisa

y se alargan las sombras,

me iré al monte de la mirra,

y al collado del incienso.

Esposo

^({versiculo}){nota al pie:?}Eres toda hermosa, amiga mía,

y no hay en ti defecto alguno.

^({versiculo}){nota al pie:?}¡Ven del Líbano, esposa mía!

¡Ven conmigo del Líbano!

¡Mira de la cima del Amaná,

de la cumbre del Senir y del Hermón,

de las guaridas de los leones,

de las montañas de los leopardos!

^({versiculo}){nota al pie:?}Me has arrebatado el corazón,

hermana mía, esposa.

Me has arrebatado el corazón

con una de tus miradas,

con una perla de tu collar.

^({versiculo}\ )¡Cuán dulce son tus amores,

hermana mía, esposa!

¡Cuánto más dulces

son tus caricias que el vino;

y la fragancia de tus perfumes

que todos los bálsamos!

^({versiculo})Miel destilan tus labios,

esposa mía,

miel y leche

hay debajo de tu lengua;

y el perfume de tus vestidos

es como el olor del Líbano.

^({versiculo}){nota al pie:?}Un huerto cerrado

es mi hermana esposa,

manantial cerrado,

fuente sellada.

^({versiculo})Tus renuevos son un vergel de granados,

con frutas exquisitas; cipro y nardo;

^({versiculo})nardo y azafrán, canela y cinamomo,

con todos los árboles de incienso;

mirra y áloes.

con todos los aromas selectos.

Esposa

^({versiculo}){nota al pie:?}La fuente del jardín

es pozo de aguas vivas,

y los arroyos fluyen del Líbano.

^({versiculo})¡Levántate, oh Aquilón,

ven, oh Austro!

¡Qué se esparzan sus aromas!

¡Venga mi amado a su jardín

y coma de sus exquisitas frutas!

 

Cantar de los Cantares 5

Esposo

^({versiculo}){nota al pie:?}Vine a mi jardín, hermana mía, esposa;

tomé de mi mirra y de mi bálsamo;

comí mi panal con mi miel;

bebí mi vino y mi leche.

¡Comed, amigos;

bebed y embriagaos, mis bien amados!

Esposa

^({versiculo}){nota al pie:?}Yo dormía,

pero mi corazón estaba despierto.

¡Una voz! Es mi amado que golpea.

Esposo

Ábreme, hermana mía, amiga mía,

paloma mía, perfecta mía,

pues mi cabeza está llena de rocío,

y mis cabellos de las gotas de la noche.

Esposa

^({versiculo}){nota al pie:?}Ya me he quitado la túnica;

¿cómo ponérmela de nuevo?

Ya me he lavado los pies;

¿cómo ensuciarlos?

^({versiculo}){nota al pie:?}Mi amado introdujo la mano por el cerrojo,

y mis entrañas todas se conmovieron.

^({versiculo}){nota al pie:?}Me levanté para abrir a mi amado,

y mis manos gotearon mirra;

de mirra exquisita

se impregnaron mis dedos

en la manecilla de la cerradura.

^({versiculo}){nota al pie:?}Abrí a mi amado,

pero mi amado, volviéndose,

había desaparecido.

Mi alma desfalleció al oír su voz.

Lo busqué y no lo hallé;

lo llamé, mas no me respondió.

^({versiculo}){nota al pie:?}Me encontraron los guardias

que hacen la ronda en la ciudad;

me golpearon, me hirieron;

y los que custodian las murallas

me quitaron el manto.

^({versiculo}){nota al pie:?}Os conjuro, oh hijas de Jerusalén,

si halláis a mi amado, decidle

que yo desfallezco de amor.

Coro

^({versiculo}\ )¿Qué es tu amado más que otro amado,

oh hermosa entre las mujeres?

¿Qué es tu amado más que los demás amados,

para que así nos conjures?

Esposa

^({versiculo}){nota al pie:?}Mi amado es blanco y rubio,

se distingue entre millares.

^({versiculo}){nota al pie:?}Su cabeza es oro puro;

sus rizos, racimos de palma,

negros como el cuervo.

^({versiculo}){nota al pie:?}Sus ojos, palomas junto a los arroyos de agua,

bañadas en leche, en pleno reposo.

^({versiculo}){nota al pie:?}Sus mejillas son eras de balsameras,

macizos de perfumadas flores;

sus labios son lirios

que destilan mirra purísima.

^({versiculo}){nota al pie:?}Sus manos son barras de oro

esmaltadas con piedras de Tarsis;

su pecho, una obra de marfil

cuajada de zafiros.

^({versiculo}){nota al pie:?}Sus piernas son columnas de mármol,

asentadas en basas de oro puro;

su aspecto es como el del Líbano,

esbelto como los cedros.

^({versiculo}){nota al pie:?}Su voz es la dulzura misma,

y todo él es amable.

Tal es mi amado, tal es mi amigo,

oh hijas de Jerusalén.

Coro

^({versiculo}){nota al pie:?}¿Adónde se ha ido tu amado,

oh hermosa entre las mujeres?

¿Hacia dónde se ha vuelto tu amado,

para que le busquemos contigo?

 

Cantar de los Cantares 6

Esposa

^({versiculo}){nota al pie:?}Mi amado bajó a su jardín,

a las eras de bálsamo,

para pastorear en los jardines,

y juntar azucenas.

^({versiculo}){nota al pie:?}Yo soy de mi amado;

y mi amado es mío,

el pastor entre azucenas.

Esposo

^({versiculo}){nota al pie:?}Hermosa eres, amiga mía, como Tirsá,

amable como Jerusalén,

temible como batallones de guerra.

^({versiculo}){nota al pie:?}Aparta de mí tus ojos,

porque ellos me conturban.

Es tu cabellera,

como una manada de cabras

que va por las laderas de Galaad.

^({versiculo})Tus dientes son como un rebaño de ovejas

que suben del lavadero,

todas con crías gemelas,

y no hay entre ellas una estéril.

^({versiculo})Como mitades de granada son tus mejillas,

detrás de tu velo.

^({versiculo}){nota al pie:?}Sesenta son las reinas,

ochenta las concubinas,

e innumerables las doncellas.

^({versiculo})Pero una es mi paloma, mi perfecta;

única para su madre,

la predilecta de aquella que la engendró.

Las jóvenes la vieron,

y la proclamaron dichosa;

la vieron las reinas y concubinas,

y la alabaron.

Coro

^({versiculo}){nota al pie:?}^(\ )¿Quién es ésta que avanza

como la aurora,

hermosa como la luna,

pura como el sol,

temible como batallones de guerra?

Esposa

^({versiculo}){nota al pie:?}He bajado al nogueral,

para mirar las flores del valle,

para ver si ha brotado la vid,

si florecen los granados.

^({versiculo}){nota al pie:?}No reconozco mi alma;

¡me ha puesto en los carros de Aminadib!

Coro

^({versiculo}){nota al pie:?}¡Vuelve, vuelve, Sulamita!

¡Vuelve, vuelve, para que te miremos!

 

Cantar de los Cantares 7

Esposa

^({versiculo}){nota al pie:?}¿Por qué miráis a la Sulamita

como las danzas de Mahanaim?

Esposo (¿Rey?)

¡Qué hermosos son tus pies

en las sandalias, hija de príncipe!

Los contornos de tus caderas son como joyas,

obra de manos de artista.

^({versiculo}){nota al pie:?}Tu seno es un tazón torneado,

en que no falta el vino sazonado.

Tu vientre es un montón de trigo

rodeado de azucenas.

^({versiculo})Como dos cervatillos son tus pechos,

gemelos de gacela.

^({versiculo}){nota al pie:?}Tu cuello es una torre de marfil,

tus ojos como las piscinas de Hesebón,

junto a la puerta de Bat-Rabim,

tu nariz como la torre del Líbano

que mira hacia Damasco.

^({versiculo}){nota al pie:?}Tu cabeza está asentada como el Carmelo,

y tu cabellera es como la púrpura:

un rey está preso en sus trenzas.

Esposo (¿o Pastor?)

^({versiculo}\ )¡Qué hermosa eres y qué encantadora,

oh amor, con tus delicias!

^({versiculo})Ese tu talle parece una palmera,

y tus pechos, racimos.

^({versiculo}){nota al pie:?}Subiré, dije yo, a la palmera,

y me asiré de sus ramas.

¡Séanme tus pechos como racimos de uvas!

Tu aliento es como manzanas,

^({versiculo}){nota al pie:?}y tu boca como vino generoso...

Esposa

que fluye suavemente para mi amado,

deslizándose entre mis labios y mis dientes.

^({versiculo}){nota al pie:?}Yo soy de mi amado

y hacia mí tienden sus deseos.

^({versiculo}){nota al pie:?} ¡Ven, amado mío,

salgamos al campo,

pasemos la noche en las aldeas!

^({versiculo})Madrugaremos para ir a las viñas;

veremos si la vid está en cierne,

si se abrieron los brotes,

si han florecido los granados.

Allí te daré mi amor.

^({versiculo}){nota al pie:?}Ya despiden su fragancia

las mandrágoras;

junto a nuestras puertas

hay toda clase de frutas exquisitas;

las nuevas y las pasadas

he guardado, amado mío, para ti.

 

Cantar de los Cantares 8

Esposa

^({versiculo}){nota al pie:?}¡Quién me diera que fueses hermano mío,

amamantado a los pechos de mi madre!

Al encontrarte afuera te besaría,

y no me despreciarían.

^({versiculo}){nota al pie:?}Yo te llamaría

y te introduciría

en la casa de mi madre;

tú me enseñarías,

y yo te daría a beber vino aromático

del zumo de granados.

^({versiculo}){nota al pie:?}Su izquierda debajo de mi cabeza,

y su derecha me abraza.

Esposo

^({versiculo})Os conjuro, hijas de Jerusalén,

que no despertéis ni inquietéis a la amada,

hasta que ella quiera.

Coro

^({versiculo}){nota al pie:?}¿Quién es ésta que sube del desierto,

apoyada sobre su amado?

Esposo

Yo te suscitaré debajo del manzano,

allí donde murió tu madre.

donde pereció la que te dio a luz.

Esposa

^({versiculo}){nota al pie:?}¡Ponme cual sello sobre tu corazón,

cual marca sobre tu brazo!

Porque es fuerte el amor

como la muerte,

e inflexibles los celos

como el infierno.

Sus flechas son flechas de fuego,

llamas del mismo Yahvé.

^({versiculo}){nota al pie:?}No valen muchas aguas

para apagar el amor,

ni los ríos pueden ahogarlo.

Si un hombre diera

todos los bienes de su casa por el amor,

sería sin embargo sumamente despreciado.

Coro

^({versiculo}){nota al pie:?}Tenemos una hermana pequeña;

no tiene pechos todavía.

¿Qué haremos con nuestra hermana

en el día en que se trate de su boda?

^({versiculo})Si es muro,

levantaremos sobre ella almenas de plata;

si es puerta,

le formaremos un tablado de cedro.

Esposa (¿O Hermana?)

^({versiculo})Muro soy,

y mis pechos son como torres.

Así he venido a ser a los ojos de él

como quien ha hallado la paz.

Coro

^({versiculo}){nota al pie:?}Una viña tenía Salomón en Baal-Hamón,

entregó la viña a los guardas;

cada uno había de darle

por sus frutos mil monedas de plata.

Esposa

^({versiculo})Tengo delante mi viña, la mía.

Para ti los mil (siclos), oh Salomón,

y doscientos para los guardas de su fruto.

Esposo

^({versiculo}){nota al pie:?}Oh tú que habitas en los jardines,

los amigos desean oír tu voz.

¡Házmela oír!

Esposa

^({versiculo}){nota al pie:?}Corre, amado mío,

y sé como la gacela y el cervatillo

sobre los montes de los bálsamos.

 

 

