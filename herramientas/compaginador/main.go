package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
)

// compaginador es una herramienta que rompe el texto crudo en su estado actual
// a diferentes archivos .tex para asistir con el proceso de digitalizacion, la
// descomposicion se hace por libros de la Biblia segun el demarcador __titulo

func main() {
	archivoEntrada := flag.String("texto-entrada", "", "texto sin procesar a ser descompuesto")
	carpetaSalida := flag.String("directorio-salida", "", "directorio para colocar los archivos .tex")

	flag.Parse()

	// verifica que el archivo de entrada existe
	ubicacionAbsEntrada, err := filepath.Abs(*archivoEntrada)
	if err != nil {
		log.Fatalf("no se pudo convertir ubicacion %s en una ubicacion absoluta: %v\n", *archivoEntrada, err)
	}
	// verifica que la carpeta de salida existe
	ubicacionAbsSalida, err := filepath.Abs(*carpetaSalida)
	if err != nil {
		log.Fatalf("no se pudo convertir ubicacion %s en una ubicacion absoluta: %v\n", *carpetaSalida, err)
	}
	ubSalida, err := os.Stat(ubicacionAbsSalida)
	if err != nil {
		log.Fatalf("no se pudo obtener el STAT de ubicacion %s: %v\n", ubicacionAbsSalida, err)
	}
	if ubSalida.Mode().IsRegular() {
		log.Fatalf("la ubicacion no es un directorio %s\n", ubicacionAbsSalida)
	}
	cuenta, err := contarLineasDeArchivo(ubicacionAbsEntrada)
	if err != nil {
		log.Fatal(err)
	}
	var lineasLeidas = 0
	var descomposicion = 0

	// abrimos el archivo con el texto crudo para su lectura
	entrada, err := os.Open(ubicacionAbsEntrada)
	if err != nil {
		log.Fatalf("no se pudo abrir archivo de entrada: %v (archivo: %s)\n", err, ubicacionAbsEntrada)
	}
	defer entrada.Close()
	scanner := bufio.NewScanner(entrada)

	var finDelArchivo error = nil
	var descSalida *os.File = nil
	// bucle infinito hasta chocar con un error o el fin del archivo
	for finDelArchivo == nil {
		// obtiene la primer linea
		r := scanner.Scan()
		if !r {
			log.Printf("scan retorno false")
			finDelArchivo = fmt.Errorf("goodbye!")
		}

		// revisa si hubo un error
		if err = scanner.Err(); err != nil {
			log.Printf("no se pudo scannear linea debido a un error: %v\n", err)
			finDelArchivo = err
		} else {
			if descSalida == nil {
				descomposicion, descSalida, err = preguntarAlUsuario("", ubicacionAbsSalida, descomposicion)
				if err != nil {
					log.Fatal(err)
				}
			}
			linea := scanner.Text()
			// comprueba si hemos encontrado otro __titulo
			if buscarTitulo.MatchString(linea) {
				if descSalida != nil {
					descSalida.Close() // cierra el antiguo descriptor
				}
				descomposicion, descSalida, err = preguntarAlUsuario(linea, ubicacionAbsSalida, descomposicion)
				if err != nil {
					log.Fatal(err)
				}
			}

			// escribe el texto al archivo actualmente abierto
			if linea == "" {
				// ignora lineas vacias
				continue
			}
			_, err = descSalida.WriteString(linea + "\n\n")
			if err != nil {
				log.Fatalf("no se pudo escribir linea de texto al destino: %v", err)
			}
			lineasLeidas += 1
			progreso := lineasLeidas * 100 / cuenta
			if progreso%5 == 0 {
				log.Printf("progreso: %03d %%...\n", progreso)
			}
		}
	}
	descSalida.Close()
}
