package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"regexp"
)

var buscarTitulo = regexp.MustCompile(`(?m)__titulo:? (.+)`)

// lineCounter cuenta cuantas lineas tiene un archivo de texto, retorna un
// error si algo salio mal.
func lineCounter(r io.Reader) (int, error) {
	buf := make([]byte, 32*1024)
	count := 0
	lineSep := []byte{'\n'}

	for {
		c, err := r.Read(buf)
		count += bytes.Count(buf[:c], lineSep)

		switch {
		case err == io.EOF:
			return count, nil

		case err != nil:
			return count, err
		}
	}
}

// contarLineasDearchivo abre un archivo y cuenta las lineas que contiene
func contarLineasDeArchivo(f string) (int, error) {
	desc, err := os.Open(f)
	if err != nil {
		return -1, err
	}
	defer desc.Close()

	cuenta, err := lineCounter(desc)
	if err != nil {
		return -1, fmt.Errorf("no se pudo contar lineas en archivo: %v (archivo: %s)", err, f)
	}
	return cuenta, nil
}

func preguntarAlUsuario(linea, ubicacion string, descomposicion int) (int, *os.File, error) {
	nombreTentativo := buscarTitulo.FindString(linea)
	fmt.Printf("nombre de archivo para %s: ", nombreTentativo)
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	nombreAdquirido := scanner.Text()
	nombre := fmt.Sprintf("%04d-%s.tex", descomposicion+1, nombreAdquirido)
	ubicacionArchivo := filepath.Join(ubicacion, nombre)
	f, err := os.Create(ubicacionArchivo)
	if err != nil {
		return -1, nil, fmt.Errorf("no se pudo abrir nuevo archivo: %v (archivo: %s)", err, ubicacionArchivo)
	}
	fmt.Printf("Guardando texto en archivo %s...\n", ubicacionArchivo)
	return descomposicion + 1, f, nil
}
