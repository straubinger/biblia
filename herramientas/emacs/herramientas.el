;; herramientas.el provee funciones útiles para trabajar con los libros de la
;; Sagrada biblia.

(setf biblia-h1-regexp (rx "__h1 " (group (one-or-more not-newline))))
(setf biblia-h2-regexp (rx "__h2 " (group (one-or-more not-newline)) space (zero-or-more (group "(" (one-or-more digit) ")"))))
(setf biblia-h3-regexp (rx "__h3 " (group (one-or-more not-newline))))
(setf biblia-titulo-regexp (rx "__titulo " (group (one-or-more not-newline))))
(setf biblia-capitulo-regepx (rx (one-or-more not-newline) (one-or-more " ") (one-or-more (or num (any "XVI"))) eol))
(setf biblia-capitulo-latex-regexp (rx (group "\\capitulo\{" (zero-or-more (not "\}")) "\}" eol)))
(setf biblia-prosa-regexp (rx ":" (one-or-more "\n")))

(defun biblia-h1-h2-h3-titulo-a-latex ()
  "cambia las demarcaciones h1 y h3 a comandos latex."
  (interactive)
  (save-excursion
    (query-replace-regexp biblia-titulo-regexp "\\\\libro\{\\1\}"))
  (save-excursion
    (query-replace-regexp biblia-h1-regexp "\\\\section*\{\\1\}"))
  (save-excursion
    (query-replace-regexp biblia-h3-regexp "\\\\subsection*\{\\1\}"))
  (save-excursion
    (query-replace-regexp biblia-h2-regexp "\\\\capitulo\{\\2\}")))

(defun biblia-reposicionar-marca-nota-al-pie ()
  "Reposiciona todas las marcas de nota al pie."
  (interactive)
  (search-forward "\\verso{}@")
  (delete-char -1)
  (search-forward "\\verso{}")
  (goto-char (- (point) (length "\\verso{}")))
  (while (looking-back (rx (not punct)) nil)
    (backward-char))
  (insert "\\footnote{}"))
