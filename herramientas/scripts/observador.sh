#!/usr/bin/env sh

echo '👀 Observando archivos *.tex,*.cls por cambios'
while true
do
    inotifywait -qq -r -e create,close_write,modify,move,delete --include '.*\.tex$' --include '.*\.cls$' "$1" && \
        tectonic "$2"
    echo "∎ fin"
done
