[![pipeline status](https://gitlab.com/straubinger/biblia/badges/master/pipeline.svg)](https://gitlab.com/straubinger/biblia/commits/master)


# Misión #

El esfuerzo de este proyecto es el de brindar al publico una versión en digital de la famosa **Biblia Platense** o **Biblia Comentada**, traducción realizada a partir de los textos originales por Monseñor Dr. Juan Straubinger. Esta traducción se caracteriza por sus ricos y explicativos comentarios al pie de pagina que el lector podrá encontrar al final de la mayoría de las paginas.

El equipo de Straubinger Digital considera que el acceso a tan preciado material debe estar al alcance de todos. Así como también evitar usar /formatos cerrados/ realizados con [software privativo](https://www.gnu.org/philosophy/proprietary/proprietary.html) como fuente para las versiones digitales de susodicho material. Es por eso que el trabajo de edición es realizado con [software libre](https://www.gnu.org/philosophy/free-sw.html), lo que garantiza que futuras generaciones de siglos por venir podrán trabajar con el material fuente y crear copias de esta Biblia para los formatos que existan en el futuro (algo que **no se puede garantizar** si el documento fuente esta en formato Microsoft Word, ya que Microsoft podría no existir más en 100 años haciendo sus productos inaccesibles y los archivos creados con ellos ilegibles).

# Descargar una copia #

Este proyecto esta apenas en su infancia, pero una copia en formato PDF y EPUB del progreso actual es ofrecido para descargar:

Descargar:
- [PDF](https://gitlab.com/straubinger/biblia/-/jobs/artifacts/master/download?job=pdf "Version ideal para imprimir en papel.")
- [EPUB](https://gitlab.com/straubinger/biblia/-/jobs/artifacts/master/download?job=epub "Version ideal para lectores electrónicos, no compatible con Kindle.")

# Construir una copia para PDF (para usuarios avanzados) #

Debe tener instalado `nix` en su sistema operativo (MacOS, GNU/Linux o WSL en Windows) o usar el sistema operativo NixOS. También debe habilitar la función experimental `flake` en `nix`. Debe iniciar la shell de desarrollo con `nix develop` o utilizar `direnv` para permitir que la shell de desarrollo sea iniciada al entrar a la carpeta raíz del proyecto.

En la raíz del proyecto, ejecute `just pdf` para ejecutar el comando especifico del proyecto para generar un archivo PDF.
